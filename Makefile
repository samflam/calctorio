build: build/production/index.js
.PHONY: build

serve: build/production/index.js
	NODE_ENV=production bun run ./server/index.js
.PHONY: serve

build-dev: build/development/index.js
.PHONY: build-dev

serve-dev: build/development/index.js
	NODE_ENV=development bun run ./server/index.js
.PHONY: serve-dev

setup:
	bun install
.PHONY: setup

typecheck tsc:
	bun run tsc -p ./client/tsconfig.json
	bun run tsc -p ./server/tsconfig.json
.PHONY: typecheck tsc

clean:
	rm -rf build
	find . -name tsconfig.tsbuildinfo -delete
.PHONY: clean

build/%/index.js: $(shell find ./client)
	NODE_ENV=$* bun run build.ts
