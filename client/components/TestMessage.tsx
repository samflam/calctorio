import React from "react";

interface Props {
	message: string
}

export default function TestMessage ({message}: Props) {
	return <div>
		<h1 style={{color: 'red'}}>{message}</h1>
	</div>
};
