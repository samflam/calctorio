import React from "react";

import TestMessage from "./TestMessage.tsx";

export default function App() {
	return <TestMessage message="Hello, world!" />
}
