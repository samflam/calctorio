import Vector from "./vector.ts";

export default class Matrix {
	readonly nrow: number;
	readonly ncol: number;
	readonly data: Float64Array;

	constructor(nrow: number, ncol: number) {
		this.nrow = nrow;
		this.ncol = ncol;
		this.data = new Float64Array(nrow * ncol);
	}

	assertIndex(idx: number) {
		if (idx !== Math.round(idx)) {
			throw "Invalid index";
		}
	}

	assertRowInBounds(row: number) {
		if (row < 0 || row >= this.nrow) {
			throw "Index out of bounds";
		}
	}

	assertColInBounds(col: number) {
		if (col < 0 || col >= this.ncol) {
			throw "Index out of bounds";
		}
	}

	getRow(row: number, check?: boolean): Vector {
		if (check !== false) {
			this.assertIndex(row);
			this.assertRowInBounds(row);
		}
		this.assertIndex(row);
		this.assertRowInBounds(row);
		return new Vector(this.data.slice(this.ncol * row, this.ncol * (row + 1)));
	}

	setRow(row: number, vector: Vector, check?: boolean): Matrix {
		if (check !== false) {
			this.assertIndex(row);
			this.assertRowInBounds(row);
		}
		if (vector.length !== this.ncol) {
			throw "Incompatible vector length";
		}
		for (let i = 0; i < vector.length; i++) {
			this.setElem(row, i, vector.data[i], false);
		}
		return this;
	}

	getCol(col: number, check?: boolean): Vector {
		if (check !== false) {
			this.assertIndex(col);
			this.assertColInBounds(col);
		}
		const result = new Vector(this.nrow);
		for (let i = 0; i < this.nrow; i++) {
			result.data[i] = this.data[col + i * this.ncol];
		}
		return result;
	}

	setCol(col: number, vector: Vector, check?: boolean): Matrix {
		if (check !== false) {
			this.assertIndex(col);
			this.assertColInBounds(col);
		}
		if (vector.length !== this.nrow) {
			throw "Incompatible vector length";
		}
		for (let i = 0; i < vector.length; i++) {
			this.setElem(i, col, vector.data[i], false);
		}
		return this;
	}

	getElem(row: number, col: number, check?: boolean): number {
		if (check !== false) {
			this.assertIndex(row);
			this.assertRowInBounds(row);
			this.assertIndex(col);
			this.assertColInBounds(col);
		}
		return this.data[row * this.ncol + col];
	}

	setElem(row: number, col: number, val: number, check?: boolean): Matrix {
		if (check !== false) {
			this.assertIndex(row);
			this.assertRowInBounds(row);
			this.assertIndex(col);
			this.assertColInBounds(col);
		}
		this.data[row * this.ncol + col] = val;
		return this;
	}

	rref(): Matrix {
		let row_idx = 0;
		let col_idx = 0;
		while(row_idx < this.nrow && col_idx < this.ncol) {
			const col = this.getCol(col_idx, false);
			let pivot_row = -1;
			let best_pivot_val = 0;
			// Using partial pivot for better numerical stability
			//   https://en.wikipedia.org/wiki/Pivot_element
			for (let i = row_idx; i < col.length; i++) {
				const val = Math.abs(col.getElem(i, false));
				if (val > best_pivot_val) {
					pivot_row = i;
					best_pivot_val = val;
				}
			}

			// No more nonzero values in this column; move to next column
			if (pivot_row === -1) {
				col_idx += 1;
				continue;
			}

			// Swap pivot row with current row
			if (pivot_row !== row_idx) {
				const tmp = this.getRow(row_idx);
				this.setRow(row_idx, this.getRow(pivot_row, false), false);
				this.setRow(pivot_row, tmp, false);
			}

			const row_vec = this.getRow(row_idx, false);
			// Scale row to get a leading one
			row_vec.timesEq(1 / row_vec.getElem(col_idx, false));
			// Ensure we have _exactly_ one, fixing any floating point weirdness
			row_vec.setElem(col_idx, 1, false);
			row_vec.collapseEpsilon();
			this.setRow(row_idx, row_vec, false);

			for (let i = 0; i < this.nrow; i++) {
				if (i === row_idx) {
					continue;
				}
				const curr_row_vec = this.getRow(i);
				const canceling_factor = -1 * curr_row_vec.getElem(col_idx, false);
				const canceler = row_vec.times(canceling_factor);
				curr_row_vec.plusEq(canceler).collapseEpsilon();
				this.setRow(i, curr_row_vec);
			}
			col_idx += 1;
			row_idx += 1;
		}

		return this;
	}
}
