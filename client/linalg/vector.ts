export default class Vector {
	readonly length: number;
	readonly data: Float64Array;

	constructor(arg: number | Vector | ArrayLike<number>) {
		if (typeof arg === "number") {
			this.data = new Float64Array(arg);
		}
		else if (arg instanceof Vector) {
			this.data = new Float64Array(arg.data);
		}
		else {
			this.data = Float64Array.from(arg);
		}
		this.length = this.data.length;
	}

	isCompatible(other: Vector): boolean {
		return this.length === other.length;
	}

	assertIndex(idx: number) {
		if (idx !== Math.round(idx)) {
			throw "Invalid index";
		}
	}

	assertCompatible(other: Vector) {
		if (!this.isCompatible(other)) {
			throw "Incompatible vector length";
		}
	}

	assertInBounds(idx: number) {
		if (idx < 0 || idx >= this.length) {
			throw "Index out of bounds";
		}
	}

	plus(other: Vector, check?: boolean): Vector {
		if (check !== false) {
			this.assertCompatible(other);
		}
		return this.copy().plusEq(other);
	}

	plusEq(other: Vector, check?: boolean): Vector {
		if (check !== false) {
			this.assertCompatible(other);
		}
		for (let i = 0; i < this.length; i++) {
			this.data[i] += other.data[i];
		}
		return this;
	}

	times(factor: number): Vector {
		return this.copy().timesEq(factor);
	}

	timesEq(factor: number): Vector {
		for (let i = 0; i < this.length; i++) {
			this.data[i] *= factor;
		}
		return this;
	}

	getElem(idx: number, check?: boolean): number {
		if (check !== false) {
			this.assertIndex(idx);
		}
		return this.data[idx];
	}

	setElem(idx: number, val: number, check?: boolean): Vector {
		if (check !== false) {
			this.assertIndex(idx);
		}
		this.data[idx] = val;
		return this;
	}

	copy(): Vector {
		return new Vector(this);
	}
	
	collapseEpsilon(): Vector {
		const epsilon = 1e-12;
		for (let i = 0; i < this.length; i++) {
			if (Math.abs(this.data[i]) < epsilon) {
				this.data[i] = 0;
			}
		}
		return this;
	}

	isZero(): boolean {
		return !this.data.some(x => x !== 0);
	}
}
