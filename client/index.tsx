import React from "react";
import { createRoot } from "react-dom/client";
import Vector from "./linalg/vector.ts";
import Matrix from "./linalg/matrix.ts";

import App from "./components/App.tsx";

const root = createRoot(document.getElementById("root")!);
root.render(<App />);

const x = new Matrix(5, 5);
x.setRow(0, new Vector([1, 2, 3, 0, 1]));
x.setRow(1, new Vector([4, 5, 6, 8, 2]));
x.setRow(2, new Vector([0, 0, 0, 0, -1]));
x.setRow(3, new Vector([1, 2, 3, 5, 4]));
x.setRow(4, new Vector([2, 1, 0, 7, 8]));
x.rref();
console.log(x.getRow(0).data);
console.log(x.getRow(1).data);
console.log(x.getRow(2).data);
console.log(x.getRow(3).data);
console.log(x.getRow(4).data);


const y = new Matrix(2, 3);
y.setRow(0, new Vector([0.00300, 59.14, 59.17]));
y.setRow(1, new Vector([5.291, -6.130, 46.78]));
y.rref();
console.log(y.getRow(0).data);
console.log(y.getRow(1).data);

const z = new Matrix(2, 3);
z.setRow(0, new Vector([5.291, -6.130, 46.78]));
z.setRow(1, new Vector([0.00300, 59.14, 59.17]));
z.rref();
console.log(z.getRow(0).data);
console.log(z.getRow(1).data);
