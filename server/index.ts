import process from "process";
import path from "path";
import express, {Express, Request, Response, NextFunction} from "express";

const app: Express = express();
const port = 3000;

app.get("/", (req: Request, res: Response) => {
	res.sendFile(path.resolve(__dirname, "static/index.html"));
});

app.get("/main.js", (req: Request, res: Response) => {
	if (process.env.NODE_ENV == "development") {
		res.sendFile(path.resolve(__dirname, "static/main-dev.js"));
		return;
	}
	res.sendFile(path.resolve(__dirname, "static/main.js"));
});

app.get("/style.css", (req: Request, res: Response) => {
	res.sendFile(path.resolve(__dirname, "static/style.css"));
});

// Unknown route handler
app.use((req: Request, res: Response) => {
	res.sendStatus(404);
});

// Global error handler
app.use((err: any, req: Request, res: Response, next: NextFunction) => {
	console.error(err);
	res.sendStatus(500);
});

app.listen(port, () => {
	console.log(`Server listening on port: ${port}`);
});
