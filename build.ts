import process from "process";

const buildResult = await Bun.build({
	entrypoints: ["./client/index.tsx"],
	outdir: `./build/${process.env.NODE_ENV}`,
	plugins: [],
});

if (!buildResult.success) {
	console.error(buildResult.logs);
}
